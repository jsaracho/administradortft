/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class ReservaData {
    private List<Reserva> data;

    public ReservaData() {
    }

    public ReservaData(List<Reserva> data) {
        this.data = data;
    }

    public List<Reserva> getData() {
        return data;
    }

    public void setData(List<Reserva> data) {
        this.data = data;
    }
    
    
}
