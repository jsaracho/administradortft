/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author johana
 */
public class BancoData {
    private List<Banco> bancos;

    public BancoData(List<Banco> bancos) {
        this.bancos = bancos;
    }

    public BancoData() {
    }

    public List<Banco> getBancos() {
        return bancos;
    }

    public void setBancos(List<Banco> bancos) {
        this.bancos = bancos;
    }
    
}
