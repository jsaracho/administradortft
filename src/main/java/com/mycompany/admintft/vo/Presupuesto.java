/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import com.mycompany.admintft.vo.Producto;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 *
 * @author user
 */
public class Presupuesto {
    private Integer id;
    private Date fecha;
    private String descripcion;
    private String usuario;
    private Integer estado;
    private BigDecimal monto;
    private BigDecimal descuento;
    private List<Producto> productos;
    private Integer cantidad;
    private Integer alto;
    private Integer largo;
    private Integer zona;
    private Integer cantHoras;
    private BigDecimal total;
    private BigDecimal sena;
    private BigDecimal costoHora;
    private String imageName;
    private String contentType;

    public Presupuesto() {
    }
    
     public Presupuesto(Integer id) {
        this.id = id;
    }

    public Presupuesto(Integer id, Date fecha, String descripcion, String usuario, Integer estado, BigDecimal monto, BigDecimal descuento) {
        this.id = id;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.usuario = usuario;
        this.estado = estado;
        this.monto = monto;
        this.descuento = descuento;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getAlto() {
        return alto;
    }

    public void setAlto(Integer alto) {
        this.alto = alto;
    }

    public Integer getLargo() {
        return largo;
    }

    public void setLargo(Integer largo) {
        this.largo = largo;
    }

    public Integer getZona() {
        return zona;
    }

    public void setZona(Integer zona) {
        this.zona = zona;
    }

    public Integer getCantHoras() {
        return cantHoras;
    }

    public void setCantHoras(Integer cantHoras) {
        this.cantHoras = cantHoras;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getSena() {
        return sena;
    }

    public void setSena(BigDecimal sena) {
        this.sena = sena;
    }

    public BigDecimal getCostoHora() {
        return costoHora;
    }

    public void setCostoHora(BigDecimal costoHora) {
        this.costoHora = costoHora;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    
}
