package com.mycompany.admintft.vo;

import java.util.Date;

/**
 *
 * @author user
 */
public class Reserva {
    private Integer id;
    private String fecha;
    private String horaDesde;
    private String horaHasta;
    private Persona cliente;
    private Persona tatuador;  
    private Presupuesto presupuesto;
    private Integer estado;

    public Reserva() {
    }

    public Reserva(Integer id, String fecha, String horaDesde, String horaHasta, 
            Persona cliente, Persona tatuador, Presupuesto presupuesto, Integer estado) {
        this.id = id;
        this.fecha = fecha;
        this.horaDesde = horaDesde;
        this.horaHasta = horaHasta;
        this.cliente = cliente;
        this.tatuador = tatuador;
        this.presupuesto = presupuesto;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraDesde() {
        return horaDesde;
    }

    public void setHoraDesde(String horaDesde) {
        this.horaDesde = horaDesde;
    }

    public String getHoraHasta() {
        return horaHasta;
    }

    public void setHoraHasta(String horaHasta) {
        this.horaHasta = horaHasta;
    }

    public Persona getCliente() {
        return cliente;
    }

    public void setCliente(Persona cliente) {
        this.cliente = cliente;
    }

    public Persona getTatuador() {
        return tatuador;
    }

    public void setTatuador(Persona tatuador) {
        this.tatuador = tatuador;
    }

    public Presupuesto getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(Presupuesto presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    
    
    
}
