/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.math.BigDecimal;

/**
 *
 * @author paolo
 */
public class ProductoPost {
    private Integer idProducto;
    private String descripcion;
    private int stock;
    private int stockMinimo;
    private BigDecimal precioUnitario;
    private Integer marca;
    private Integer categoria;
    private String medida;
    private String unidadMedida;
    private String estado;
    private String descripcionEstado;
    private Integer color;
    private BigDecimal precioCompra;

    public ProductoPost() {
    }

    public ProductoPost(Integer idProducto, String descripcion, int stock, int stockMinimo, BigDecimal precioUnitario, Integer marca, Integer categoria, String medida, String unidadMedida, String estado, String descripcionEstado, Integer color, BigDecimal precioCompra) {
        this.idProducto = idProducto;
        this.descripcion = descripcion;
        this.stock = stock;
        this.stockMinimo = stockMinimo;
        this.precioUnitario = precioUnitario;
        this.marca = marca;
        this.categoria = categoria;
        this.medida = medida;
        this.unidadMedida = unidadMedida;
        this.estado = estado;
        this.descripcionEstado = descripcionEstado;
        this.color = color;
        this.precioCompra = precioCompra;
    }

    
    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getMarca() {
        return marca;
    }

    public void setMarca(Integer marca) {
        this.marca = marca;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }

    public BigDecimal getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(BigDecimal precioCompra) {
        this.precioCompra = precioCompra;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }


    
}
