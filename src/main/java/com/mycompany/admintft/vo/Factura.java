package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author johana
 */
public class Factura implements Serializable {

    private Integer idFactura;
     private String rucCliente;
    private String nombreCliente;
    private String apellidoCliente;
    private String nroFactura;
    private String descripcion;
    private Date fecha;
    private String estado;
    private BigDecimal importe;
    private BigDecimal iva;
    private Timbrado timbrado;
    private List<DetalleFactura> detalles;
    private List<FormaPago> formas;
    private Integer idReserva;
            

    public Factura() {
    }

    public Factura(Integer idFactura, String nroFactura, String descripcion, Date fecha, String estado,
            BigDecimal importe, BigDecimal iva, Timbrado timbrado, List<DetalleFactura> detalles) {
        this.idFactura = idFactura;
        this.nroFactura = nroFactura;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.estado = estado;
        this.importe = importe;
        this.iva = iva;
        this.timbrado = timbrado;
        this.detalles = detalles;
    }

    public String getRucCliente() {
        return rucCliente;
    }

    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    
    
    
    public Factura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    
   
    public Timbrado getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(Timbrado timbrado) {
        this.timbrado = timbrado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFactura != null ? idFactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Factura)) {
            return false;
        }
        Factura other = (Factura) object;
        if ((this.idFactura == null && other.idFactura != null) || (this.idFactura != null && !this.idFactura.equals(other.idFactura))) {
            return false;
        }
        return true;
    }

    public List<DetalleFactura> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleFactura> detalles) {
        this.detalles = detalles;
    }

    public List<FormaPago> getFormas() {
        return formas;
    }

    public void setFormas(List<FormaPago> formas) {
        this.formas = formas;
    }

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }
    
    
    
    @Override
    public String toString() {
        return "py.com.mcs.bms.bean.Factura[ idFactura=" + idFactura + " ]";
    }
    
}
