package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class PersonaData {
    private List<Persona> personas;

    public PersonaData() {
    }

    public PersonaData(List<Persona> personas) {
        this.personas = personas;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
    
    
}
