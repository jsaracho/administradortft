/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author paolo
 */
public class AjusteStockData {
    
    private List<AjusteStock> ajusteStock;

    public AjusteStockData(List<AjusteStock> ajusteStock) {
        this.ajusteStock = ajusteStock;
    }

    public AjusteStockData() {
    }

    public List<AjusteStock> getAjusteStock() {
        return ajusteStock;
    }

    public void setAjusteStock(List<AjusteStock> ajusteStock) {
        this.ajusteStock = ajusteStock;
    }
    
    
    
    
    
}
