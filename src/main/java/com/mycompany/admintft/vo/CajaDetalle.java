/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author johana
 */
public class CajaDetalle implements Serializable {

    protected Integer item;
    private Date fecha;
    private BigInteger ingreso;
    private BigInteger egreso;
    private String detalle;
    private Caja caja;
    private Factura factura;

    public CajaDetalle() {
    }


    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getIngreso() {
        return ingreso;
    }

    public void setIngreso(BigInteger ingreso) {
        this.ingreso = ingreso;
    }

    public BigInteger getEgreso() {
        return egreso;
    }

    public void setEgreso(BigInteger egreso) {
        this.egreso = egreso;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Caja getCaja() {
        return caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    
    
}
