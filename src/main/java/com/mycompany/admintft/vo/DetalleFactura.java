/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author johana
 */
public class DetalleFactura implements Serializable {

    private Integer idFactura;
    private int item;
    private BigDecimal importe;
    private int cantidad;
    private BigDecimal precioUnitario;
    private BigDecimal iva;
    private Factura factura;
    private Producto producto;

    public DetalleFactura(Integer idFactura, int item, BigDecimal importe, int cantidad, BigDecimal iva, Factura factura, Producto producto) {
        this.idFactura = idFactura;
        this.item = item;
        this.importe = importe;
        this.cantidad = cantidad;
        this.iva = iva;
        this.factura = factura;
        this.producto = producto;
    }
    
    public DetalleFactura() {
    }

    public DetalleFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

   

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }
    
    

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFactura != null ? idFactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleFactura)) {
            return false;
        }
        DetalleFactura other = (DetalleFactura) object;
        if ((this.idFactura == null && other.idFactura != null) || (this.idFactura != null && !this.idFactura.equals(other.idFactura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.mcs.bms.bean.DetalleFactura[ idFactura=" + idFactura + " ]";
    }
    
}
