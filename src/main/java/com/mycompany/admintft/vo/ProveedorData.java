/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author johana
 */
public class ProveedorData {
    private List<Proveedor> list;

    public ProveedorData(List<Proveedor> list) {
        this.list = list;
    }

    public ProveedorData() {
    }
    
    

    public List<Proveedor> getList() {
        return list;
    }

    public void setList(List<Proveedor> list) {
        this.list = list;
    }
    
    
}
