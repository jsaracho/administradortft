/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author user
 */
public class MarcaData implements Serializable{
    private List<Marca> marcas;

    public MarcaData() {
    }

    public MarcaData(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }
    
    
}
