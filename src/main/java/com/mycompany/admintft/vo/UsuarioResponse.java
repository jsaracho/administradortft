/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class UsuarioResponse {
    private Integer idUsuario;
    private String usuario;
    private String contrasena;
    private String estado;
    private Persona persona;
    private List<Rol> roles;
    private List<Pantalla> pantallas;

    public UsuarioResponse() {
    }

    public UsuarioResponse(Integer idUsuario, String usuario, String contrasena, Persona persona, List<Rol> roles, List<Pantalla> pantallas) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.persona = persona;
        this.roles = roles;
        this.pantallas = pantallas;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public List<Pantalla> getPantallas() {
        return pantallas;
    }

    public void setPantallas(List<Pantalla> pantallas) {
        this.pantallas = pantallas;
    }

    @Override
    public String toString() {
        return "UsuarioResponse{" + "idUsuario=" + idUsuario + ", usuario=" + usuario + ", contrasena=" + contrasena + ", persona=" + persona + ", roles=" + roles + ", pantallas=" + pantallas + '}';
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    

}
