/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;

/**
 *
 * @author johana
 */
public class Pantalla implements Serializable {

    private Integer idPantalla;
    private String descripcion;

    public Pantalla() {
    }

    public Pantalla(Integer idPantalla) {
        this.idPantalla = idPantalla;
    }

    public Pantalla(Integer idPantalla, String descripcion) {
        this.idPantalla = idPantalla;
        this.descripcion = descripcion;
    }

    public Integer getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(Integer idPantalla) {
        this.idPantalla = idPantalla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPantalla != null ? idPantalla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pantalla)) {
            return false;
        }
        Pantalla other = (Pantalla) object;
        if ((this.idPantalla == null && other.idPantalla != null) || (this.idPantalla != null && !this.idPantalla.equals(other.idPantalla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.mcs.bms.bean.Pantalla[ idPantalla=" + idPantalla + " ]";
    }
    
}
