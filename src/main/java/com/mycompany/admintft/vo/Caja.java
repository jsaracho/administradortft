/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 *
 * @author johana
 */
public class Caja implements Serializable {

    private Integer idCaja;
    private int idUsuario;
    private Date fecha;
    private BigInteger montoApertura;
    private BigInteger montoCierre;
    private List<CajaDetalle> cajaDetalles;
    private String estado;

    public Caja() {
    }

    public Caja(Integer idCaja) {
        this.idCaja = idCaja;
    }

    public Caja(Integer idCaja, int idUsuario, Date fecha) {
        this.idCaja = idCaja;
        this.idUsuario = idUsuario;
        this.fecha = fecha;
    }

    public Integer getIdCaja() {
        return idCaja;
    }

    public void setIdCaja(Integer idCaja) {
        this.idCaja = idCaja;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getMontoApertura() {
        return montoApertura;
    }

    public void setMontoApertura(BigInteger montoApertura) {
        this.montoApertura = montoApertura;
    }

    public BigInteger getMontoCierre() {
        return montoCierre;
    }

    public void setMontoCierre(BigInteger montoCierre) {
        this.montoCierre = montoCierre;
    }

    public List<CajaDetalle> getCajaDetalles() {
        return cajaDetalles;
    }

    public void setCajaDetalles(List<CajaDetalle> cajaDetalles) {
        this.cajaDetalles = cajaDetalles;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
