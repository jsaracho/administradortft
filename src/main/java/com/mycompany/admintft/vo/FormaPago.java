/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author johana
 */
public class FormaPago implements Serializable {

    private BigDecimal importe;
    private String forma;
    private String formaDescripcion;
    private String codigoPost;
    private String codigoCheque;
    private Factura  factura;
    private Banco idBanco;
    private String tipo;

    public FormaPago() {
    }


    public FormaPago( BigDecimal importe, String forma, String formaDescripcion) {
        this.importe = importe;
        this.forma = forma;
        this.formaDescripcion = formaDescripcion;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    public String getFormaDescripcion() {
        return formaDescripcion;
    }

    public void setFormaDescripcion(String formaDescripcion) {
        this.formaDescripcion = formaDescripcion;
    }

    public String getCodigoPost() {
        return codigoPost;
    }

    public void setCodigoPost(String codigoPost) {
        this.codigoPost = codigoPost;
    }

    public String getCodigoCheque() {
        return codigoCheque;
    }

    public void setCodigoCheque(String codigoCheque) {
        this.codigoCheque = codigoCheque;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Banco getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Banco idBanco) {
        this.idBanco = idBanco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
}
