/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class ParamData {
    private List<Param> data;

    public ParamData() {
    }

    public ParamData(List<Param> data) {
        this.data = data;
    }

    public List<Param> getData() {
        return data;
    }

    public void setData(List<Param> data) {
        this.data = data;
    }
    
    
}
