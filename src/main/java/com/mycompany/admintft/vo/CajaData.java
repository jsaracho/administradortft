/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class CajaData {
     private List<CajaDetalle> data;

    public CajaData() {
    }

    public CajaData(List<CajaDetalle> data) {
        this.data = data;
    }
     

    public List<CajaDetalle> getData() {
        return data;
    }

    public void setData(List<CajaDetalle> data) {
        this.data = data;
    }
     
     
}
