package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class ProductoData {
    private List<Producto> productos;

    public ProductoData(List<Producto> productos) {
        this.productos = productos;
    }

    public ProductoData() {
    }
    
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    
}
