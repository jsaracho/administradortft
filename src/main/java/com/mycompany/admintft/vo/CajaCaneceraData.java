/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class CajaCaneceraData {
    private List<Caja> data;

    public CajaCaneceraData(List<Caja> data) {
        this.data = data;
    }

    public CajaCaneceraData() {
    }
    
    

    public List<Caja> getData() {
        return data;
    }

    public void setData(List<Caja> data) {
        this.data = data;
    }
    
    
}
