/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class CategoriaData {
    
   private List<Categoria> data;

    public CategoriaData() {
    }

    public CategoriaData(List<Categoria> data) {
        this.data = data;
    }
    

    public List<Categoria> getData() {
        return data;
    }

    public void setData(List<Categoria> data) {
        this.data = data;
    }
   
   
    
}
