/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class HorarioYFeriado {
    private String entrada;
    private String entradaAlmuerzo;
    private String salida;
    private String salidaAlmuerzo;
    private List<Feriado> feriados;

    public HorarioYFeriado() {
    }
    

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getEntradaAlmuerzo() {
        return entradaAlmuerzo;
    }

    public void setEntradaAlmuerzo(String entradaAlmuerzo) {
        this.entradaAlmuerzo = entradaAlmuerzo;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getSalidaAlmuerzo() {
        return salidaAlmuerzo;
    }

    public void setSalidaAlmuerzo(String salidaAlmuerzo) {
        this.salidaAlmuerzo = salidaAlmuerzo;
    }

    public List<Feriado> getFeriados() {
        return feriados;
    }

    public void setFeriados(List<Feriado> feriados) {
        this.feriados = feriados;
    }
    
    
}
