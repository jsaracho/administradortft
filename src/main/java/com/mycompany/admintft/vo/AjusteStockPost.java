/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.Date;

/**
 *
 * @author paolo
 */
public class AjusteStockPost {
    private Integer idAjusteStock;
    private Date fecha;
    private String descripcion;
    private String usuario;
    private int cantAnterior;
    private int cantActual;
    private Integer producto;
    private Integer idMotivo;
    private String tipo;
    private Integer cantidad;
    
    
    public AjusteStockPost() {
    }

    public AjusteStockPost(Integer idAjusteStock, Date fecha, String descripcion, String usuario, int cantAnterior, int cantActual, Integer producto) {
        this.idAjusteStock = idAjusteStock;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.usuario = usuario;
        this.cantAnterior = cantAnterior;
        this.cantActual = cantActual;
        this.producto = producto;
    }

    public Integer getIdAjusteStock() {
        return idAjusteStock;
    }

    public void setIdAjusteStock(Integer idAjusteStock) {
        this.idAjusteStock = idAjusteStock;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getCantAnterior() {
        return cantAnterior;
    }

    public void setCantAnterior(int cantAnterior) {
        this.cantAnterior = cantAnterior;
    }

    public int getCantActual() {
        return cantActual;
    }

    public void setCantActual(int cantActual) {
        this.cantActual = cantActual;
    }

    public Integer getProducto() {
        return producto;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
    
    
    
    
    
}
