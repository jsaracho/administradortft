/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author johana
 */
public class AjusteStock implements Serializable {

    private Integer idAjusteStock;
    private Date fecha;
    private String descripcion;
    private String usuario;
    private int cantAnterior;
    private int cantActual;
    private Producto producto;

    
    public AjusteStock() {
    }

    public AjusteStock(Integer idAjusteStock) {
        this.idAjusteStock = idAjusteStock;
    }

    public AjusteStock(Integer idAjusteStock, Date fecha, String descripcion, String usuario, int cantAnterior, int cantActual, Producto producto) {
        this.idAjusteStock = idAjusteStock;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.usuario = usuario;
        this.cantAnterior = cantAnterior;
        this.cantActual = cantActual;
        this.producto = producto;
    }

    
    
    public Integer getIdAjusteStock() {
        return idAjusteStock;
    }

    public void setIdAjusteStock(Integer idAjusteStock) {
        this.idAjusteStock = idAjusteStock;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getCantAnterior() {
        return cantAnterior;
    }

    public void setCantAnterior(int cantAnterior) {
        this.cantAnterior = cantAnterior;
    }

    public int getCantActual() {
        return cantActual;
    }

    public void setCantActual(int cantActual) {
        this.cantActual = cantActual;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAjusteStock != null ? idAjusteStock.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AjusteStock)) {
            return false;
        }
        AjusteStock other = (AjusteStock) object;
        if ((this.idAjusteStock == null && other.idAjusteStock != null) || (this.idAjusteStock != null && !this.idAjusteStock.equals(other.idAjusteStock))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.mcs.bms.bean.AjusteStock[ idAjusteStock=" + idAjusteStock + " ]";
    }
    
}
