/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.util.List;

/**
 *
 * @author user
 */
public class FacturaData {
    private List<Factura> data;

    public FacturaData() {
    }

    public FacturaData(List<Factura> data) {
        this.data = data;
    }

    
    public List<Factura> getData() {
        return data;
    }

    public void setData(List<Factura> data) {
        this.data = data;
    }
    
    
}
