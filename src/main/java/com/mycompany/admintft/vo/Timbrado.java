/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author johana
 */
public class Timbrado implements Serializable {

    private Integer idTimbrado;
    private String nroTimbrado;
    private Date fechaInicio;
    private Date fechaFin;
    private int nroDesde;
    private int nroHasta;
    private String activo;

    public Timbrado() {
    }

    public Timbrado(Integer idTimbrado) {
        this.idTimbrado = idTimbrado;
    }

    public Timbrado(Integer idTimbrado, String nroTimbrado, Date fechaInicio, Date fechaFin, int nroDesde, int nroHasta, String activo) {
        this.idTimbrado = idTimbrado;
        this.nroTimbrado = nroTimbrado;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.nroDesde = nroDesde;
        this.nroHasta = nroHasta;
        this.activo = activo;
    }

    public Integer getIdTimbrado() {
        return idTimbrado;
    }

    public void setIdTimbrado(Integer idTimbrado) {
        this.idTimbrado = idTimbrado;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getNroDesde() {
        return nroDesde;
    }

    public void setNroDesde(int nroDesde) {
        this.nroDesde = nroDesde;
    }

    public int getNroHasta() {
        return nroHasta;
    }

    public void setNroHasta(int nroHasta) {
        this.nroHasta = nroHasta;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTimbrado != null ? idTimbrado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Timbrado)) {
            return false;
        }
        Timbrado other = (Timbrado) object;
        if ((this.idTimbrado == null && other.idTimbrado != null) || (this.idTimbrado != null && !this.idTimbrado.equals(other.idTimbrado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.mcs.bms.bean.Timbrado[ idTimbrado=" + idTimbrado + " ]";
    }
    
}
