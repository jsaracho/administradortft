/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.HorarioYFeriado;
import com.mycompany.admintft.vo.Param;
import com.mycompany.admintft.vo.ParamData;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class ParamService {

    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/util/";

    public ParamData getParam(String urlPath) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ParamData response = restTemplate.getForObject(
                    URL + urlPath,
                    ParamData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public HorarioYFeriado getHorarioYFeriado(){
        try {
            RestTemplate restTemplate = new RestTemplate();
            HorarioYFeriado response = restTemplate.getForObject(
                    URL + "/horario",
                    HorarioYFeriado.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public Integer save(Param param, String urlPath) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL + urlPath, param, Integer.class);
        return response;
    }

    public void update(Param param, String urlPath) {
        try {
//            /v1/util/update/color
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForObject(URL + "update/"+urlPath, param, Integer.class);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
