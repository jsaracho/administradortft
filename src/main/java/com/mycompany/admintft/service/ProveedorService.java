/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.CategoriaData;
import com.mycompany.admintft.vo.Persona;
import com.mycompany.admintft.vo.Producto;
import com.mycompany.admintft.vo.ProductoPost;
import com.mycompany.admintft.vo.Proveedor;
import com.mycompany.admintft.vo.ProveedorData;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class ProveedorService {

    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/proveedores";

    public ProveedorData getProvedores() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProveedorData response = restTemplate.getForObject(
                    URL,
                    ProveedorData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public CategoriaData getCategorias() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            CategoriaData response = restTemplate.getForObject(
                    URL + "/categorias",
                    CategoriaData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public Integer save(Proveedor proveedor) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL, proveedor, Integer.class);
        return response;
    }

    public void update(Proveedor proveedor) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Integer response = restTemplate.postForObject(URL +"/update", proveedor, Integer.class);
        } catch (Exception e) {
            System.out.println("actualizar datos del proveedor");
            System.out.println(e);
        }
    }
}
