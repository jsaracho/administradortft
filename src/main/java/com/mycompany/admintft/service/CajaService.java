/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.Caja;
import com.mycompany.admintft.vo.CajaCaneceraData;
import com.mycompany.admintft.vo.CajaData;
import com.mycompany.admintft.vo.CajaDetalle;
import com.mycompany.admintft.vo.Persona;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class CajaService {
    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/caja";

    public CajaData getCajaDetalleData(Integer tipo) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            CajaData response = restTemplate.getForObject(
                    URL+"?tipo="+tipo,
                    CajaData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public Caja getCierre(Integer usuario) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Caja response = restTemplate.getForObject(
                    URL+"/cierre?idUsuario="+usuario,
                    Caja.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public CajaCaneceraData getCajaData(Integer usuario) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            CajaCaneceraData response = restTemplate.getForObject(
                    URL+"?usuario="+usuario,
                    CajaCaneceraData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    

    public Integer save(CajaDetalle detalle, Integer idCaja) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"?idCaja="+idCaja, detalle, Integer.class);
        return response;
    }
    
     public Integer saveCaja(Caja caja, Integer idCabecera) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"/caja"+"?idCaja="+idCabecera, caja, Integer.class);
        return response;
    }
    
    public Integer saveCabecera(Integer usuario) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"/cabecera"+"?usuario="+usuario, null, Integer.class);
        return response;
    }

    public void update(Persona persona) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Integer response = restTemplate.postForObject(URL +"/update", persona, Integer.class);
        } catch (Exception e) {
            System.out.println("actualizar datos del persona");
            System.out.println(e);
        }
    }
}
