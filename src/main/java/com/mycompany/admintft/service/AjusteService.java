/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.AjusteStockData;
import com.mycompany.admintft.vo.AjusteStockPost;
import com.mycompany.admintft.vo.CajaDetalle;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */

public class AjusteService {
     private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/ajuste-stock";
     
    public Integer save(AjusteStockPost ajuste) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL, ajuste, Integer.class);
        return response;
    }
    
    public AjusteStockData getAjustes() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            AjusteStockData response = restTemplate.getForObject(
                    URL,
                    AjusteStockData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
}
