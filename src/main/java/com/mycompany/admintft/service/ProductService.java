/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.CategoriaData;
import com.mycompany.admintft.vo.ColorData;
import com.mycompany.admintft.vo.MarcaData;
import com.mycompany.admintft.vo.Producto;
import com.mycompany.admintft.vo.ProductoData;
import com.mycompany.admintft.vo.ProductoPost;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class ProductService {

    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/productos";

    public ProductoData getProductos() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL ,
                    ProductoData.class);
            List<Producto> p= response.getProductos();
            List<Producto> filtrado= new ArrayList();
            for(Producto prd:p ){
                if(prd.getRequerido()!=null && prd.getRequerido().equals("SI") && prd.getStock()>prd.getStockMinimo()){
                    filtrado.add(prd);
                }
            }
            response.setProductos(filtrado);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ProductoData getProductosSinFiltro() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL ,
                    ProductoData.class);
            List<Producto> p= response.getProductos();
            response.setProductos(p);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ProductoData getAllProductos() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL ,
                    ProductoData.class);
            List<Producto> p= response.getProductos();
             List<Producto> filtrado= new ArrayList();
            for(Producto prd:p ){
                if(prd.getStock()>prd.getStockMinimo()){
                    filtrado.add(prd);
                }
            }
            response.setProductos(filtrado);
            return response;
        } catch (Exception e) {
            return new ProductoData(new ArrayList());
        }
    }
    
    public ProductoData getProductos(String estado) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL +"?estado="+estado,
                    ProductoData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public void updateStock(Integer stock, Integer id) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put(
                    URL +"/stock/"+ id + "?stock="+stock,
                    null);
        } catch (Exception e) {
        }
    }
    
    public MarcaData getMarcaData() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            MarcaData response = restTemplate.getForObject(
                    URL + "/marca",
                    MarcaData.class);
            return response;
        } catch (Exception e) {
            return new MarcaData();
        }
    }

    public CategoriaData getCategoriaData() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            CategoriaData response = restTemplate.getForObject(
                    URL + "/categorias",
                    CategoriaData.class);
            return response;
        } catch (Exception e) {
            return new CategoriaData();
        }
    }

    public ColorData getColorData() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ColorData response = restTemplate.getForObject(
                    URL + "/colores",
                    ColorData.class);
            return response;
        } catch (Exception e) {
            return new ColorData();
        }
    }

    public ProductoData getProductoData(String tipo) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL + "?estado=" + tipo,
                    ProductoData.class);
             List<Producto> filtrado= new ArrayList();
            for(Producto prd:response.getProductos() ){
                if(prd.getRequerido()!=null && prd.getRequerido().equals("SI")){
                    filtrado.add(prd);
                }
            }
            response.setProductos(filtrado);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public ProductoData getProductoStock(String tipo) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL + "?tipo=" + tipo,
                    ProductoData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public Producto getProductoById(Integer id) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Producto response = restTemplate.getForObject(
                    URL + "/" + id,
                    Producto.class);
            return response;
        } catch (Exception e) {
            System.out.println("Error producto by id");
            System.out.println(e);
            return null;
        }
    }

    public Integer save(ProductoPost producto) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL, producto, Integer.class);
        return response;
    }

    public void update(ProductoPost producto) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put(URL + "/" + producto.getIdProducto(), producto);
        } catch (Exception e) {
            System.out.println("Guardar producto");
            System.out.println(e);
        }
    }
    
    public void update(Integer producton, Integer stock) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put(URL + "/stock/" + producton + "?stock="+stock, null);
        } catch (Exception e) {
            System.out.println("Guardar producto");
            System.out.println(e);
        }
    }

    public ProductoData getProductoDataCode(String code) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ProductoData response = restTemplate.getForObject(
                    URL,
                    ProductoData.class);
            List<Producto> productos = new ArrayList<>();
            for (Producto prod : response.getProductos()) {
                if (prod.getDescripcion().toUpperCase().contains(code.toUpperCase())) {
                    productos.add(prod);
                }
            }
            System.out.println("Prod");
            System.out.println(productos);
            response.setProductos(productos);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

}
