/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.Persona;
import com.mycompany.admintft.vo.Reserva;
import com.mycompany.admintft.vo.ReservaData;
import java.util.ArrayList;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class ReservaService {

    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/reservas";

    public ReservaData getReservas() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ReservaData response = restTemplate.getForObject(
                    URL,
                    ReservaData.class);
            return response;
        } catch (Exception e) {
            return new  ReservaData(new ArrayList());
        }
    }

    public Persona getDisponible(Reserva r) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Persona response = restTemplate.postForObject(URL + "/disponibilidad", r, Persona.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public Integer save(Reserva reserva, String usuario) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String query = "?user=" + usuario;
            Integer response = restTemplate.postForObject(URL + query, reserva, Integer.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public void update(Reserva reserva, String usuario) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String query = "?user=" + usuario;
            restTemplate.postForObject(URL + "/update" + query, reserva, Integer.class);
        } catch (Exception e) {
            System.out.println("Error al guardar reserva");
            System.out.println(e);
        }
    }

}
