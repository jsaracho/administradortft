/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.Persona;
import com.mycompany.admintft.vo.PersonaData;
import java.util.Date;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class ClienteService {

    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/persona";

    public PersonaData getPersonaData(Integer tipo) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            PersonaData response = restTemplate.getForObject(
                    URL+"?tipo="+tipo,
                    PersonaData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }


    public Integer save(Persona persona) {
        RestTemplate restTemplate = new RestTemplate();
        persona.setCedula(persona.getCedula());
        persona.setFechaNacimiento(new Date());
        Integer response = restTemplate.postForObject(URL+ "?tipo="+1, persona, Integer.class);
        return response;
    }

    public void update(Persona persona) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Integer response = restTemplate.postForObject(URL +"/update", persona, Integer.class);
        } catch (Exception e) {
            System.out.println("actualizar datos del persona");
            System.out.println(e);
        }
    }
}
