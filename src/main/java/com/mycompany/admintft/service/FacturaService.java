/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.BancoData;
import com.mycompany.admintft.vo.Factura;
import com.mycompany.admintft.vo.FacturaData;
import com.mycompany.admintft.vo.FormaPago;
import java.util.List;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class FacturaService {
    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/comprobantes";

    public FacturaData getFacturas() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            FacturaData response = restTemplate.getForObject(
                    URL ,
                    FacturaData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public FacturaData getFacturasCompra() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            FacturaData response = restTemplate.getForObject(
                    URL+"/compra" ,
                    FacturaData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
     public FacturaData getFacturasVenta() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            FacturaData response = restTemplate.getForObject(
                    URL ,
                    FacturaData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
     
      public List<String> getFacturasEstados(Integer presu) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            List<String> response = restTemplate.getForObject(
                    URL +"/estados?id="+presu,
                    List.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    public Integer save(Factura factura) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL, factura, Integer.class);
        return response;
    }
    
    public Integer saveCompra(Factura factura) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"/compra", factura, Integer.class);
        return response;
    }
    
   public void anularCompra(Integer factura) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(URL+"/compra/" + factura, null, Void.class);
    }
    
     public Integer update(String estado, Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"/update?estado="+estado+"&id="+id, null, Integer.class);
        return response;
    }
    
    
    public Integer saveFormaPago(Integer factura, List<FormaPago> formaPago) {
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"/detalle_pago/"+factura , formaPago, Integer.class);
        return response;
    }
    
    public Integer saveFormaPagoCompra(Integer factura, List<FormaPago> formaPago) {
        for(FormaPago f: formaPago){
            f.setTipo("C");
        }
        RestTemplate restTemplate = new RestTemplate();
        Integer response = restTemplate.postForObject(URL+"/detalle_pago/compra/"+factura , formaPago, Integer.class);
        return response;
    }
    
    public BancoData getBancos(){
        try {
            RestTemplate restTemplate = new RestTemplate();
            BancoData response = restTemplate.getForObject(
                    "http://localhost:8180/mcs.tft-1.0.0/v1/bancos/" ,
                    BancoData.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    
    
}
