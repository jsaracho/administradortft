/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.service;

import com.mycompany.admintft.vo.ReservaData;
import com.mycompany.admintft.vo.UsuarioResponse;
import java.util.ArrayList;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class UsuarioService {
    
    private final String URL = "http://localhost:8180/mcs.tft-1.0.0/v1/usuarios";

    public UsuarioResponse getUsuario(String usuario) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            UsuarioResponse response = restTemplate.getForObject(
                    URL+"/"+ usuario,
                    UsuarioResponse.class);
            return response;
        } catch (Exception e) {
            return null;
        }
    }
}
