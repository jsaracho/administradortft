/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ReservaService;
import com.mycompany.admintft.service.UsuarioService;
import com.mycompany.admintft.vo.Pantalla;
import com.mycompany.admintft.vo.Reserva;
import com.mycompany.admintft.vo.UsuarioResponse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class LoginBean implements Serializable {

    private String usuario;
    private String contraseña;
    private UsuarioResponse usuarioR;
    private List<Reserva> reservas;

    public String usuarioValido() {
        try {
            reservas = new ArrayList();
            ReservaService reservaService = new ReservaService();
            UsuarioService service = new UsuarioService();
            UsuarioResponse us = service.getUsuario(usuario);
            if (us.getUsuario().equals(usuario) && us.getContrasena().equals(contraseña)) {
                usuarioR = us;
                List<Reserva> rrr = reservaService.getReservas().getData();
                for (Reserva r : rrr) {
                    if (r.getTatuador().getIdPersona().equals(usuarioR.getPersona().getIdPersona()) && r.getEstado() == 1) {
                        reservas.add(r);
                    }
                }
                if (reservas.isEmpty()) {
                    reservas = null;
                }
                return "home";
            }

            return "login";
        } catch (Exception e) {
            return "login";
        }

    }

    public boolean tienePermiso(String pantalla) {
        try {
            int count = 0;
            for (Pantalla p : usuarioR.getPantallas()) {
                if (p.getDescripcion().equals(pantalla)) {
                    count++;
                }
            }
            return count > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public String logout() {
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).invalidate();
        return "login";
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public UsuarioResponse getUsuarioR() {
        return usuarioR;
    }

    public void setUsuarioR(UsuarioResponse usuarioR) {
        this.usuarioR = usuarioR;
    }

    public List<Reserva> getReservas() {
        return reservas;
    }

    public void setReservas(List<Reserva> reservas) {
        this.reservas = reservas;
    }

}
