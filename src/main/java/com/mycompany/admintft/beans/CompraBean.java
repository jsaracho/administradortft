/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.FacturaService;
import com.mycompany.admintft.service.ProductService;
import com.mycompany.admintft.vo.Banco;
import com.mycompany.admintft.vo.CajaDetalle;
import com.mycompany.admintft.vo.DetalleFactura;
import com.mycompany.admintft.vo.Factura;
import com.mycompany.admintft.vo.FormaPago;
import com.mycompany.admintft.vo.Producto;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class CompraBean implements Serializable {

    private List<Producto> productos;
    private List<Producto> productosSeleccionados;
    private Producto producto;
    ProductService service = new ProductService();
    private Integer cantidad;
    FacturaService facturaService = new FacturaService();
    Factura factura;
    private List<Factura> facturas;
    private boolean mensaje;
    List<FormaPago> formasPago;
    private BigDecimal monto;
    FormaPago forma;
    private String formaTipo;
    String formaMsg;

    @Inject
    CajaBean cajaBean;

    @PostConstruct
    public void init() {
        forma = new FormaPago();
        mensaje = false;
        facturas = new ArrayList<>();
        factura = new Factura();
        factura.setDescripcion("Compra de articulos varios");
        productosSeleccionados = new ArrayList<>();
        producto = new Producto();
        productos = service.getProductos("A").getProductos();
    }

    public String cargarFacturas() {
        facturas = facturaService.getFacturasCompra().getData();
        return "facturaCompraList";
    }
    
    public String home() {
        return "home";
    }
    
    public String fechaString() {
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        return " ".concat(sdff.format(factura.getFecha()));
    }

    public void select(Producto p) {
        this.producto = p;
    }

    public String verCompra() {
        init();
        return "compra";
    }

    public String verFactura() {
        if (factura != null && factura.getEstado().equals("P")) {
            formaMsg = null;
            formasPago = new ArrayList();
            monto= factura.getImporte();
        }
        mensaje = false;
        return "facturaCompra";
    }

    public void add() {
        if (producto != null && producto.getIdProducto() != null && cantidad != null) {
            producto.setCant(cantidad);
            productos.remove(producto);
            productosSeleccionados.add(producto);
            producto = new Producto();
            cantidad = null;
        }

    }

    public void remove(Producto p) {
        if (p != null && p.getIdProducto() != null) {
            productosSeleccionados.remove(p);
            productos.add(p);
            producto = new Producto();
        }
    }

    public String comprar() {
        List<DetalleFactura> detalles = new ArrayList();
        Integer item = 1;
        BigDecimal bd = BigDecimal.ZERO;
        for (Producto p : productosSeleccionados) {
            detalles.add(new DetalleFactura(null, item, p.getPrecioCompra(), p.getCant(),
                    p.getPrecioCompra().divide(new BigDecimal("11"), 0, 0), null, p));
            service.updateStock(p.getStock() + p.getCant(), p.getIdProducto());
            bd = bd.add(p.getPrecioCompra().multiply(new BigDecimal(p.getCant().toString())));
            item++;
        }
        factura.setDetalles(detalles);
        factura.setEstado("P");
        factura.setFecha(new Date());
        factura.setImporte(bd);
        factura.setIva(bd.divide(new BigDecimal("11"), 0, 0));
        Integer id = facturaService.saveCompra(factura);
        factura.setIdFactura(id);
        mensaje = true;
        return "facturaCompraFinalizar";
    }

    public String guardarFormaPago() {
        if (cajaBean.getIdCaja() == null) {
            formaMsg = "Error, no se puede pagar la factura, no se hecho la apertura de caja";
            return null;
        }
        BigDecimal montoAcumulado = BigDecimal.ZERO;
        if (formasPago == null || formasPago.isEmpty()) {
            formaMsg = "Favor complete la forma de pago";
        } else {
            for (FormaPago fp : formasPago) {
                montoAcumulado = montoAcumulado.add(fp.getImporte());
            }
            if (montoAcumulado.equals(factura.getImporte())) {
                facturaService.saveFormaPagoCompra(factura.getIdFactura(), formasPago);
                formaMsg = null;
                facturas = facturaService.getFacturasCompra().getData();
                CajaDetalle cajaDetalle = new CajaDetalle();
                cajaDetalle.setDetalle(factura.getDescripcion());
                cajaDetalle.setEgreso(factura.getImporte().toBigInteger());
                cajaDetalle.setFactura(factura);
                cajaDetalle.setFecha(new Date());
                cajaDetalle.setIngreso(BigInteger.ZERO);
                cajaBean.saveDetalle(cajaDetalle);
                return "facturaCompraList";
            } else {
                formaMsg = "Error al guardar Factura, el monto acumulado no coincide con el monto total de la factura";
            }
        }

        return null;
    }

    public String getFormaDescription(String forma) {
        if (forma.equals("E")) {
            return "Efectivo";
        }
        if (forma.equals("C")) {
            return "Cheque";
        }
        if (forma.equals("TD")) {
            return "Tarjeta de debito";
        }
        if (forma.equals("TC")) {
            return "Tarjeta de credito";
        }
        return "";
    }

    public void addForma() {
        BigDecimal montoAcumulado = BigDecimal.ZERO;
        if (monto.equals(BigDecimal.ZERO)) {
            formaMsg = "El monto debe ser mayor a cero";
        } else {
            if (formasPago != null && !formasPago.isEmpty()) {
                for (FormaPago fp : formasPago) {
                    montoAcumulado = montoAcumulado.add(fp.getImporte());
                }
                montoAcumulado = montoAcumulado.add(monto);
                if (montoAcumulado.equals(factura.getImporte()) || montoAcumulado.compareTo(factura.getImporte()) < 0) {
                    FormaPago forma = new FormaPago(monto, formaTipo, getFormaDescription(formaTipo));
                    forma.setTipo("C");
                    formasPago.add(forma);
                    monto = null;
                    formaTipo = null;
                    formaMsg = null;
                } else {
                    formaMsg = "Ya se esta superando el importe de la factura";
                }
            } else {
                montoAcumulado = montoAcumulado.add(monto);
                if (formasPago == null) {
                    formasPago = new ArrayList();
                }
                if (montoAcumulado.equals(factura.getImporte()) || montoAcumulado.compareTo(factura.getImporte()) < 0) {
                    formasPago.add(new FormaPago(monto, formaTipo, getFormaDescription(formaTipo)));
                    monto = null;
                    formaTipo = null;
                    formaMsg = null;
                } else {
                    formaMsg = "Ya se esta superando el importe de la factura";
                }

            }
        }

    }

    public String anular() {
        if (cajaBean.getIdCaja() == null) {
            formaMsg = "Error, no se puede anular la factura, no se hecho la apertura de caja";
            return null;
        }
        if (factura.getEstado().equals("P")) {
            facturaService.anularCompra(factura.getIdFactura());
            facturas = facturaService.getFacturasCompra().getData();
            return "facturaCompraList";
        } else {
            CajaDetalle cajaDetalle = new CajaDetalle();
            cajaDetalle.setDetalle("Se anula la factura nro: " + factura.getIdFactura());
            cajaDetalle.setEgreso(BigInteger.ZERO);
            cajaDetalle.setFactura(factura);
            cajaDetalle.setFecha(new Date());
            cajaDetalle.setIngreso(factura.getImporte().toBigInteger());
            cajaBean.saveDetalle(cajaDetalle);
            facturaService.anularCompra(factura.getIdFactura());
            facturas = facturaService.getFacturasCompra().getData();
            return "facturaCompraList";
        }

    }

    public String getFormatDate(Date date) {
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy/MM/dd");
        return sdff.format(date);
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public List<Producto> getProductosSeleccionados() {
        return productosSeleccionados;
    }

    public void setProductosSeleccionados(List<Producto> productosSeleccionados) {
        this.productosSeleccionados = productosSeleccionados;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }

    public boolean isMensaje() {
        return mensaje;
    }

    public void setMensaje(boolean mensaje) {
        this.mensaje = mensaje;
    }

    public List<FormaPago> getFormasPago() {
        return formasPago;
    }

    public void setFormasPago(List<FormaPago> formasPago) {
        this.formasPago = formasPago;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public FormaPago getForma() {
        return forma;
    }

    public void setForma(FormaPago forma) {
        this.forma = forma;
    }

    public String getFormaTipo() {
        return formaTipo;
    }

    public void setFormaTipo(String formaTipo) {
        this.formaTipo = formaTipo;
    }

    public String getFormaMsg() {
        return formaMsg;
    }

    public void setFormaMsg(String formaMsg) {
        this.formaMsg = formaMsg;
    }

    public String getDateString(){
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        return sdff.format(new Date());
    }
    
    public String getDateString(Date date){
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        return sdff.format(date);
    }
}
