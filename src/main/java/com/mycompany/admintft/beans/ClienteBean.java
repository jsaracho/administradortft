package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ClienteService;
import com.mycompany.admintft.vo.Persona;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class ClienteBean implements Serializable {

    List<Persona> clientes;
    ClienteService service;
    Persona cliente;

    @PostConstruct
    public void init() {
        cliente = new Persona();
        service = new ClienteService();
        clientes = service.getPersonaData(1) != null ? service.getPersonaData(1).getPersonas() : new ArrayList();
    }

    public String verClientes() {
        init();
        return "clientes";
    }

    public String save() {
        if (cliente.getIdPersona() != null) {
            service.update(cliente);
        } else {
            service.save(cliente);
        }
        clientes = service.getPersonaData(1) != null ? service.getPersonaData(1).getPersonas() : new ArrayList();
        return "clientes";
    }

    public String delete() {
        service.update(cliente);
        clientes = service.getPersonaData(1) != null ? service.getPersonaData(1).getPersonas() : new ArrayList();
        return "clientes";
    }

    public String add() {
        return "clientesNew";
    }

    public String edit() {
        return "clientesNew";
    }

    public List<Persona> getClientes() {
        return clientes;
    }

    public void setClientes(List<Persona> clientes) {
        this.clientes = clientes;
    }

    public Persona getCliente() {
        return cliente;
    }

    public void setCliente(Persona cliente) {
        this.cliente = cliente;
    }

}
