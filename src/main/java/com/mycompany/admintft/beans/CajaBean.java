/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.CajaService;
import com.mycompany.admintft.vo.Caja;
import com.mycompany.admintft.vo.CajaDetalle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class CajaBean implements Serializable {

    private Caja caja;
     private List<Caja> cajas;
    private CajaDetalle detalle;
    private List<CajaDetalle> detalles;
    @Inject
    LoginBean loginBean;
    CajaService service;
    Integer idCaja;
    
    @PostConstruct
    public void init() {
        service= new CajaService();
        detalles = new ArrayList<>();
        caja= new Caja();
        cajas= service.getCajaData(loginBean.getUsuarioR().getIdUsuario()).getData();
        for(Caja c: cajas){
            if(c.getEstado().equals("A")){
                idCaja= c.getIdCaja();
                caja= c;
                detalles= c.getCajaDetalles();
            }
        }
    }
    
    public String abrirCaja(){
        caja.setFecha(new Date());
        caja.setIdUsuario(loginBean.getUsuarioR().getIdUsuario());
        idCaja= service.saveCaja(caja, 0);
        return "cajaList";
    }
    
    public String cierre(){
        Caja cajaCierre= service.getCierre(loginBean.getUsuarioR().getIdUsuario());
        caja.setMontoCierre(cajaCierre.getMontoCierre());
        caja.setEstado(cajaCierre.getEstado());
        cajas=null;
        idCaja=null;
        detalles=null;
        caja=null;
        return "caja";
    }
    
    public String verCaja(){
        if(idCaja==null){
            caja= new Caja();
            return "caja";
        }
        cajas= service.getCajaData(loginBean.getUsuarioR().getIdUsuario()).getData();
        for(Caja c: cajas){
            if(c.getEstado().equals("A")){
                idCaja= c.getIdCaja();
                caja= c;
                detalles= c.getCajaDetalles();
            }
        }
        return "cajaList";
    }
    
    public String verCajaCierre(){
        if(idCaja==null){
            return "caja";
        }
        cajas= service.getCajaData(loginBean.getUsuarioR().getIdUsuario()).getData();
        for(Caja c: cajas){
            if(c.getEstado().equals("A")){
                idCaja= c.getIdCaja();
                caja= c;
                detalles= c.getCajaDetalles();
            }
        }
        return "cajaCierreList";
    }
    
    public void saveDetalle(CajaDetalle cajaDetalle){
        Caja c= new Caja(idCaja);
        c.setIdUsuario(loginBean.getUsuarioR().getIdUsuario());
        cajaDetalle.setCaja(c);
        service.save(cajaDetalle, idCaja);
    }
    
    public Caja getCaja() {
        return caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    public CajaDetalle getDetalle() {
        return detalle;
    }

    public void setDetalle(CajaDetalle detalle) {
        this.detalle = detalle;
    }

    public List<CajaDetalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<CajaDetalle> detalles) {
        this.detalles = detalles;
    }

    public Integer getIdCaja() {
        return idCaja;
    }

    public void setIdCaja(Integer idCaja) {
        this.idCaja = idCaja;
    }
    
}
