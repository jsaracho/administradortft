/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ParamService;
import com.mycompany.admintft.vo.Param;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class MarcaBean implements Serializable{
    private Param param;
    private List<Param> params;
    ParamService service;

    @PostConstruct
    public void init() {
        param = new Param();
        service = new ParamService();
        params = service.getParam("marca").getData();
    }
    
      public String verMarca() {
        init();
        return "marca";
    }

    public String save() {
        if (param.getId() == null) {
            service.save(param, "marca");
        } else {
            service.update(param, "marca");
        }
        params = service.getParam("marca").getData();
        return "marca";
    }

    public String add() {
        param = new Param();
        return "marcaNew";
    }

    public String edit() {
        return "marcaNew";
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }
}
