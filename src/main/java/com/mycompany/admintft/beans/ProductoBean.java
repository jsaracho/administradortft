package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.AjusteService;
import com.mycompany.admintft.service.ParamService;
import com.mycompany.admintft.service.ProductService;
import com.mycompany.admintft.vo.AjusteStock;
import com.mycompany.admintft.vo.AjusteStockPost;
import com.mycompany.admintft.vo.Categoria;
import com.mycompany.admintft.vo.Param;
import com.mycompany.admintft.vo.Producto;
import com.mycompany.admintft.vo.ProductoPost;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class ProductoBean implements Serializable {

    private List<Param> colores;
    private List<Param> marcas;
    private List<Categoria> categorias;
    private List<Producto> productos;
    private List<Producto> productosSinFiltro;
    private Producto producto;
    ProductService service = new ProductService();
    ParamService paramService = new ParamService();
    private Integer idMarca;
    private Integer idCategoria;
    private Integer idColor;
    private boolean editar;
    private AjusteStockPost ajusteStockPost;
    @Inject
    LoginBean loginBean;
    AjusteService ajusteService;
    List<AjusteStock> ajustes;
    private List<Param> motivos;

    @PostConstruct
    public void init() {
        motivos = new ArrayList();
        ajusteService = new AjusteService();
        ajustes = ajusteService.getAjustes().getAjusteStock();
        ajusteStockPost = new AjusteStockPost();
        producto = new Producto();
        productos = service.getProductos().getProductos();
        colores = paramService.getParam("color").getData();
        marcas = paramService.getParam("marca").getData();
        motivos = paramService.getParam("motivo_ajuste").getData();
        categorias = service.getCategoriaData().getData();
        productosSinFiltro = service.getProductosSinFiltro().getProductos();
    }

    public String verProductos() {
        init();
        productos = service.getProductos().getProductos();
        return "producto";
    }

    public String ajusteStockList() {
        init();
        return "ajusteStockList";
    }

    public String delete() {
        ProductoPost post = new ProductoPost();
        post.setCategoria(idCategoria);
        post.setColor(idColor);
        post.setDescripcionEstado(producto.getDescripcionEstado());
        post.setDescripcion(producto.getDescripcion());
        post.setEstado("I");
        post.setMarca(idMarca);
        post.setMedida(producto.getMedida());
        post.setUnidadMedida(producto.getUnidadMedida());
        post.setPrecioCompra(producto.getPrecioCompra());
        post.setPrecioUnitario(producto.getPrecioUnitario());
        post.setStock(producto.getStock());
        post.setStockMinimo(producto.getStockMinimo());
        post.setIdProducto(producto.getIdProducto());
        service.update(post);
        productos = service.getProductos().getProductos();
        return "producto";
    }

    public String save() {
        ProductoPost post = new ProductoPost();
        post.setCategoria(idCategoria);
        post.setColor(idColor);
        post.setDescripcionEstado(producto.getDescripcionEstado());
        post.setDescripcion(producto.getDescripcion());
        post.setEstado("A");
        post.setMarca(idMarca);
        post.setMedida(producto.getMedida());
        post.setUnidadMedida(producto.getUnidadMedida());
        post.setPrecioCompra(producto.getPrecioCompra());
        post.setPrecioUnitario(producto.getPrecioUnitario());
        post.setStock(0);
        post.setStockMinimo(producto.getStockMinimo());
        if (producto.getIdProducto() == null) {
            service.save(post);
        } else {
            post.setIdProducto(producto.getIdProducto());
            service.update(post);
        }
        productos = service.getProductos().getProductos();
        return "producto";
    }

    public String saveAjuste() {
        if (ajusteStockPost.getTipo() != null && ajusteStockPost.getIdMotivo() != null && ajusteStockPost.getDescripcion() != null
                && !ajusteStockPost.getDescripcion().equals("")) {
            ajusteStockPost.setCantAnterior(producto.getStock());
            if (ajusteStockPost.getTipo().equals("Ingreso")) {
                producto.setStock(producto.getStock()+ ajusteStockPost.getCantidad());
            } else {
                producto.setStock(producto.getStock() - ajusteStockPost.getCantidad());
                if (producto.getStock() < 0) {
                    return "ajusteStock";
                }
            }
            ajusteStockPost.setCantActual(producto.getStock());
            ajusteService.save(ajusteStockPost);
            service.update(producto.getIdProducto(), producto.getStock());
            ajustes = ajusteService.getAjustes().getAjusteStock();
            productosSinFiltro = service.getProductosSinFiltro().getProductos();
            return "ajusteStockList";
        }
        return "ajusteStock";
    }

    public String newProducto() {
        producto = new Producto();
        editar = false;
        return "productoNew";
    }

    public String edit() {
        idMarca = producto.getMarca() != null ? producto.getMarca().getIdMarca() : null;
        idCategoria = producto.getCategoria() != null ? producto.getCategoria().getIdCategoria() : null;
        idColor = producto.getColor() != null ? producto.getColor().getIdColor() : null;
        editar = true;
        return "productoNew";
    }

    public String ajusteStock() {
        ajusteStockPost = new AjusteStockPost();
        idMarca = producto.getMarca() != null ? producto.getMarca().getIdMarca() : null;
        idCategoria = producto.getCategoria() != null ? producto.getCategoria().getIdCategoria() : null;
        idColor = producto.getColor() != null ? producto.getColor().getIdColor() : null;
        editar = false;
        ajusteStockPost.setCantAnterior(producto.getStock());
        ajusteStockPost.setFecha(new Date());
        ajusteStockPost.setProducto(producto.getIdProducto());
        ajusteStockPost.setUsuario(loginBean.getUsuario());
        return "ajusteStock";
    }

    public String eliminar() {
        return "productoNew";
    }

    public List<Param> getColores() {
        return colores;
    }

    public void setColores(List<Param> colores) {
        this.colores = colores;
    }

    public List<Param> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Param> marcas) {
        this.marcas = marcas;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Integer idMarca) {
        this.idMarca = idMarca;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdColor() {
        return idColor;
    }

    public void setIdColor(Integer idColor) {
        this.idColor = idColor;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    public AjusteStockPost getAjusteStockPost() {
        return ajusteStockPost;
    }

    public void setAjusteStockPost(AjusteStockPost ajusteStockPost) {
        this.ajusteStockPost = ajusteStockPost;
    }

    public List<AjusteStock> getAjustes() {
        return ajustes;
    }

    public void setAjustes(List<AjusteStock> ajustes) {
        this.ajustes = ajustes;
    }

    public List<Producto> getProductosSinFiltro() {
        return productosSinFiltro;
    }

    public void setProductosSinFiltro(List<Producto> productosSinFiltro) {
        this.productosSinFiltro = productosSinFiltro;
    }

    public AjusteService getAjusteService() {
        return ajusteService;
    }

    public void setAjusteService(AjusteService ajusteService) {
        this.ajusteService = ajusteService;
    }

    public List<Param> getMotivos() {
        return motivos;
    }

    public void setMotivos(List<Param> motivos) {
        this.motivos = motivos;
    }

}
