/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ParamService;
import com.mycompany.admintft.vo.Param;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class CategoriaBean implements Serializable {

    private Param param;
    private List<Param> params;
    ParamService service;

    @PostConstruct
    public void init() {
        param = new Param();
        service = new ParamService();
        params = service.getParam("categoria").getData();
    }

    public String verCategorias() {
        init();
        return "categoria";
    }

    public String save() {
        if (param.getId() == null) {
            service.save(param, "categoria");
        } else {
            service.update(param, "categoria");
        }
        params = service.getParam("categoria").getData();
        return "categoria";
    }

    public String add() {
        param = new Param();
        return "categoriaNew";
    }

    public String edit() {
        return "categoriaNew";
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }

}
