/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ProveedorService;
import com.mycompany.admintft.vo.Categoria;
import com.mycompany.admintft.vo.Persona;
import com.mycompany.admintft.vo.Proveedor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class ProveedorBean implements Serializable {

    List<Proveedor> proveedors;
    Proveedor proveedor;
    List<Categoria> categorias;
    List<Categoria> categoriasSeleccionadas;
    ProveedorService service;
    Persona persona;

    @PostConstruct
    public void init() {
        persona = new Persona();
        categoriasSeleccionadas = new ArrayList<>();
        proveedor = new Proveedor();
        proveedor.setPersona(new Persona());
        service = new ProveedorService();
        categorias = service.getCategorias().getData();
        proveedors = service.getProvedores()!= null ? service.getProvedores().getList(): new ArrayList();
    }
    
    public String verProveedores(){
        init();
        return "proveedor";
    }

    public String save() {
        proveedor.setPersona(persona);
        proveedor.setCategorias(categoriasSeleccionadas);
        if (proveedor.getPersona() != null && proveedor.getIdProveedor() != null) {
            service.update(proveedor);
        } else {
            service.save(proveedor);
        }
        proveedors = service.getProvedores().getList();
        return "proveedor";
    }

    public String delete() {
        proveedor.setEstado(0);
        return save();
    }

    public void addCategoria(Categoria c) {
        categoriasSeleccionadas.add(c);
        categorias.remove(c);
    }

    public void eliminarCategoria(Categoria c) {
        categorias.add(c);
        categoriasSeleccionadas.remove(c);
    }

    public String add() {
        categorias = service.getCategorias().getData();
        categoriasSeleccionadas= new ArrayList();
        proveedor= new Proveedor();
        persona= new Persona();
        return "proveedorNew";
    }

    public String edit() {
        categorias = service.getCategorias().getData();
        categoriasSeleccionadas = proveedor.getCategorias();
        for (Categoria cat : categoriasSeleccionadas) {
            categorias.remove(cat);
        }
        return "proveedorNew";
    }

    public List<Proveedor> getProveedors() {
        return proveedors;
    }

    public void setProveedors(List<Proveedor> proveedors) {
        this.proveedors = proveedors;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public List<Categoria> getCategoriasSeleccionadas() {
        return categoriasSeleccionadas;
    }

    public void setCategoriasSeleccionadas(List<Categoria> categoriasSeleccionadas) {
        this.categoriasSeleccionadas = categoriasSeleccionadas;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

}
