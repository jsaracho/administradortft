/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ParamService;
import com.mycompany.admintft.vo.Param;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class ColorBean implements Serializable {

    private Param param;
    private List<Param> params;
    ParamService service;

    @PostConstruct
    public void init() {
        param = new Param();
        service = new ParamService();
        params = service.getParam("color").getData();
    }
    
     public String verColor() {
        init();
        return "color";
    }

    public String save() {
        if (param.getId() == null) {
            service.save(param, "color");
        } else {
            service.update(param, "color");
        }
        params = service.getParam("color").getData();
        return "color";
    }

    public String add() {
        param = new Param();
        return "colorNew";
    }

    public String edit() {
        return "colorNew";
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }

}
