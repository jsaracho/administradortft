/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ParamService;
import com.mycompany.admintft.vo.Param;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class ModoPagoBean implements Serializable{
    private Param param;
    private List<Param> params;
    ParamService service;

    @PostConstruct
    public void init() {
        param = new Param();
        service = new ParamService();
        params = service.getParam("modo_pago").getData();
    }
    
     public String verModoPago() {
        init();
        return "modoPago";
    }

    public String save() {
        if (param.getId() == null) {
            service.save(param, "modo_pago");
        } else {
            service.update(param, "modo_pago");
        }
        params = service.getParam("modo_pago").getData();
        return "modoPago";
    }

    public String add() {
        param = new Param();
        return "modoPagoNew";
    }

    public String edit() {
        return "modoPagoNew";
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }
}
