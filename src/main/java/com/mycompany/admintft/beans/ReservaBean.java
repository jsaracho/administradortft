package com.mycompany.admintft.beans;

import com.mycompany.admintft.service.ClienteService;
import com.mycompany.admintft.service.FacturaService;
import com.mycompany.admintft.service.ParamService;
import com.mycompany.admintft.service.ProductService;
import com.mycompany.admintft.service.ReservaService;
import com.mycompany.admintft.vo.Banco;
import com.mycompany.admintft.vo.CajaDetalle;
import com.mycompany.admintft.vo.DetalleFactura;
import com.mycompany.admintft.vo.Factura;
import com.mycompany.admintft.vo.Feriado;
import com.mycompany.admintft.vo.FormaPago;
import com.mycompany.admintft.vo.HorarioYFeriado;
import com.mycompany.admintft.vo.Param;
import com.mycompany.admintft.vo.Persona;
import com.mycompany.admintft.vo.Presupuesto;
import com.mycompany.admintft.vo.Producto;
import com.mycompany.admintft.vo.Reserva;
import com.mycompany.admintft.vo.Timbrado;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.model.file.UploadedFileWrapper;
import org.primefaces.shaded.commons.io.FilenameUtils;

/**
 *
 * @author user
 */
@Model
@SessionScoped
public class ReservaBean implements Serializable {
    
    private List<Reserva> reservas;
    ReservaService service;
    Reserva reserva;
    Presupuesto presupuesto;
    @Inject
    LoginBean loginBean;
    private List<Producto> colors;
    private List<Param> zonas;
    private Integer zonaSeleccionada;
    private List<Producto> coloresSeleccionados;
    private boolean verBloqueGenerar;
    private List<Producto> productos;
    private ParamService paramService;
    private CroppedImage croppedImage;
    private List<Persona> clientes;
    private Persona cliente;
    private UploadedFile originalImageFile;
    private ClienteService clienteService;
    private Date horaDesde;
    private int estaDisponible;
    private int paneles;
    private Date fecha;
    private Date horaHasta;
    private boolean fechadesdeMayorhasta;
    private boolean fechaMenoraHoy;
    ProductService serviceproducto = new ProductService();
    private Boolean procesar;
    private Boolean ver;
    private Boolean finalizar;
    private Boolean cancelar;
    private boolean mostrarMensaje;
    FacturaService facturaService = new FacturaService();
    Factura factura;
    private String warningProcesar = "";
    private boolean mostrarMsg = false;
    List<Factura> facturas;
    List<FormaPago> formasPago;
    private BigDecimal monto;
    FormaPago forma;
    List<Banco> bancos;
    private String formaTipo;
    String formaMsg;
    @Inject
    CajaBean cajaBean;
    private boolean anular;
    private boolean esferiado;
    private boolean fuerahorario;
    private boolean findesemana;
    private Date date;
    
    public void init() {
        forma = new FormaPago();
        formasPago = new ArrayList<>();
        facturas = new ArrayList<>();
        factura = new Factura();
        originalImageFile = null;
        zonaSeleccionada = null;
        horaDesde = null;
        horaHasta = null;
        fecha = null;
        paneles = 0;
        procesar = false;
        ver = false;
        finalizar = false;
        cancelar = false;
        clienteService = new ClienteService();
        service = new ReservaService();
        reserva = new Reserva();
        paramService = new ParamService();
        zonas = paramService.getParam("zona").getData();
        verBloqueGenerar = false;
        presupuesto = new Presupuesto();
        coloresSeleccionados = new ArrayList<>();
        productos = new ArrayList<>();
        clientes = clienteService.getPersonaData(1).getPersonas();
        cliente = new Persona();
        estaDisponible = 0;
        reservas = service.getReservas().getData();
        fechadesdeMayorhasta = false;
        fechaMenoraHoy = false;
        colors = serviceproducto.getAllProductos().getProductos();
        List<Producto> ps = new ArrayList();
        for (Producto p : colors) {
            if (p.getCategoria().getIdCategoria() == 3) {
                ps.add(p);
            }
        }
        colors = ps;
    }
    
    public String gotoReserva() {
        init();
        return "reservaList";
    }
    
    public String gotoReservaS() {
        init();
        return "reserva";
    }
    
    public String loadFacturas() {
        facturas = facturaService.getFacturasVenta().getData();
        return "facturaVentaList";
    }
    
    public void save() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        reserva.setFecha(sdf.format(fecha));
        reserva.setHoraHasta(sdf.format(horaHasta));
        reserva.setHoraDesde(sdf.format(horaDesde));
        reserva.setCliente(cliente);
        presupuesto.setFecha(fecha);
        presupuesto.setImageName(originalImageFile.getFileName());
        presupuesto.setContentType(originalImageFile.getContentType());
        if (presupuesto.getProductos() != null) {
            presupuesto.getProductos().addAll(coloresSeleccionados);
        } else {
            presupuesto.setProductos(coloresSeleccionados);
        }
        reserva.setPresupuesto(presupuesto);
        if (reserva.getId() == null) {
            Timbrado timbrado = new Timbrado();
            List<DetalleFactura> detalles = new ArrayList();
            detalles.add(new DetalleFactura(null, 0, presupuesto.getSena(), 1, BigDecimal.ONE, null, null));
            factura = new Factura(null, null, "Por el 50% del servicio de tatuaje", new Date(),
                    "P", presupuesto.getSena(), BigDecimal.ONE,
                    timbrado, detalles);
            service.save(reserva, loginBean.getUsuario());
            factura.setIdFactura(1);
        } else {
            service.update(reserva, loginBean.getUsuario());
        }
        reservas = service.getReservas().getData();
        fechadesdeMayorhasta = false;
        siguientePanel();
    }
    
    public void saveImage() {
        try {
            Path folder = Paths.get("/src/main/resources/images");
            String filename = FilenameUtils.getBaseName(originalImageFile.getFileName());
            String extension = FilenameUtils.getExtension(originalImageFile.getFileName());
            Path file = Files.createTempFile(folder, filename + "-", "." + extension);
        } catch (Exception e) {
        }
    }
    
    public String fechaString() {
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        return " ".concat(sdff.format(factura.getFecha()));
    }
    
    public void upLoad2(FileUploadEvent event) {
        try {
            HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            if (!upload2(servletRequest, event.getFile())) {
                
            }
        } catch (Exception e) {
        }
    }
    
    public boolean upload2(HttpServletRequest request, UploadedFile archivo) {
        try {
            BufferedInputStream in = null;
            BufferedOutputStream output = null;
            try {
                String realName = archivo.getFileName();
                File dir = new File("C:\\temp\\imagenes");
                if (!dir.exists()) {
                    if (dir.mkdir()) {
                        (new File(dir.getAbsolutePath())).mkdir();
                    }
                }
                File f = new File(dir.getAbsolutePath() + "\\" + realName);
                in = new BufferedInputStream(archivo.getInputStream());
                output = new BufferedOutputStream(new FileOutputStream(f));
                byte[] buf = new byte[1024];
                long length;
                while ((length = in.read(buf)) != -1) {
                    output.write(buf, 0, (int) length);
                }
                this.originalImageFile = archivo;
                return true;
            } catch (Exception e) {
            } finally {
                if (in != null) {
                    in.close();
                }
                if (output != null) {
                    output.close();
                }
            }
        } catch (Exception e) {
        }
        
        return false;
    }
    
    public StreamedContent getImage2() {
        
        InputStream stream;
        try {
            if (originalImageFile == null) {
                stream = new FileInputStream(new File("C:\\temp\\imagenes\\" + presupuesto.getImageName()));
                return DefaultStreamedContent.builder().contentType(presupuesto.getContentType()).name(presupuesto.getImageName()).stream(() -> stream).build();
            } else {
                stream = new FileInputStream(new File("C:\\temp\\imagenes\\" + originalImageFile.getFileName()));
                return DefaultStreamedContent.builder().contentType(originalImageFile.getContentType()).name(originalImageFile.getFileName()).stream(() -> stream).build();
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    public String verVer() {
        edit();
        ver = true;
        procesar = false;
        mostrarMsg = false;
        finalizar = false;
        cancelar = false;
        return "reservaViewStatus";
    }
    
    public String verprocesar() {
        edit();
        ver = false;
        procesar = true;
        finalizar = false;
        cancelar = false;
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat horaformat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        if (horaformat.format(new Date()).substring(13, 15).equals(reserva.getHoraDesde())) {
            warningProcesar = "";
        } else {
            warningProcesar = "Atención, la hora actual y la hora de la reserva no coinciden";
            mostrarMsg = true;
        }
        List<String> estados = facturaService.getFacturasEstados(reserva.getPresupuesto().getId());
        if (estados == null || estados.isEmpty()) {
            warningProcesar = "No se puede PROCESAR la reserva, no cuenta con Factura asociada";
            mostrarMsg = true;
            procesar = false;
        }
        for (String e : estados) {
            if (e.equals("P")) {
                warningProcesar = "No se puede PROCESAR la reserva, la factura se encuentra pendiente de pago";
                mostrarMsg = true;
                procesar = false;
            }
            if (e.equals("C")) {
                warningProcesar = "No se puede PROCESAR la reserva, la factura se encuentra cancelada";
                mostrarMsg = true;
                procesar = false;
            }
        }
        if (!reserva.getFecha().equals(sdff.format(new Date()))) {
            warningProcesar = "No se puede PROCESAR la reserva porque la fecha de reserva no es igual a la fecha actual";
            mostrarMsg = true;
            procesar = false;
        }
        return "reservaViewStatus";
    }
    
    public String verFinaliza() {
        edit();
        ver = false;
        procesar = false;
        finalizar = true;
        cancelar = false;
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat horaformat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        if (horaformat.format(new Date()).substring(13, 15).equals(reserva.getHoraDesde())) {
            warningProcesar = "";
        } else {
            warningProcesar = "Atención, la hora actual y la hora de la reserva no coinciden";
            mostrarMsg = true;
        }
        if (!reserva.getFecha().equals(sdff.format(new Date()))) {
            warningProcesar = "No se puede FINALIZAR la reserva porque la fecha de reserva no es igual a la fecha actual";
            mostrarMsg = true;
            finalizar = false;
        }
        return "reservaViewStatus";
    }
    
    public String verCancelar() {
        edit();
        mostrarMsg = false;
        ver = false;
        procesar = false;
        finalizar = false;
        cancelar = true;
        return "reservaViewStatus";
    }
    
    public String señarUpdate() {
        reserva.setEstado(2);
        service.update(reserva, loginBean.getUsuario());
        reservas = service.getReservas().getData();
        return "reservaList";
    }
    
    public String procesarUpdate() {
        reserva.setEstado(4);
        service.update(reserva, loginBean.getUsuario());
        reservas = service.getReservas().getData();
        return "reservaList";
    }
    
    public String finalizarUpdate() {
        reserva.setEstado(3);
        service.update(reserva, loginBean.getUsuario());
        Timbrado timbrado = new Timbrado();
        List<DetalleFactura> detalles = new ArrayList();
        detalles.add(new DetalleFactura(null, 0, presupuesto.getMonto().divide(new BigDecimal("2")), 1, BigDecimal.ONE, null, null));
        factura = new Factura(null, null, "Por el 50% restante del servicio de tatuaje", new Date(),
                "P", presupuesto.getMonto().divide(new BigDecimal("2")), BigDecimal.ONE,
                timbrado, detalles);
        reservas = service.getReservas().getData();
        return "facturaVentaFinalizar";
    }
    
    public String cancelarUpdate() {
        reserva.setEstado(5);
        service.update(reserva, loginBean.getUsuario());
        reservas = service.getReservas().getData();
        return "reservaList";
    }
    
    public void siguientePanel() {
        paneles = paneles + 1;
    }
    
    public String siguientePanelSimulador() {
        paneles = paneles + 1;
        return "reserva";
    }
    
    public boolean verifyHorario(HorarioYFeriado horario) {
        Integer entrada = Integer.valueOf(horario.getEntrada());
        Integer entradaAlmuerzo = Integer.valueOf(horario.getEntradaAlmuerzo());
        Integer salida = Integer.valueOf(horario.getSalida());
        Integer salidaAlmuerzo = Integer.valueOf(horario.getSalidaAlmuerzo());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String horaHastaString = sdf.format(horaHasta);
        horaHastaString = horaHastaString.substring(11).replace(":", "");
        String horaDesdeString = sdf.format(horaDesde);
        horaDesdeString = horaDesdeString.substring(11).replace(":", "");
        Integer horaDesdeInt = Integer.valueOf(horaDesdeString);
        Integer horaHastaInt = Integer.valueOf(horaHastaString);
        if (horaDesdeInt > entrada && horaDesdeInt < salida
                && (horaDesdeInt < salidaAlmuerzo || horaDesdeInt > entradaAlmuerzo)) {
            if (horaHastaInt > entrada && horaHastaInt < salida
                    && (horaHastaInt < salidaAlmuerzo || horaHastaInt > entradaAlmuerzo)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    
    public void verificarDisponibilidad() {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7) {
            findesemana = true;
            fuerahorario = false;
            esferiado = false;
            estaDisponible = 0;
            return;
        }
        
        Date fechaHoy = new Date();
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy/MM/dd");
        HorarioYFeriado horario = paramService.getHorarioYFeriado();
        
        if (verifyHorario(horario)) {
            fuerahorario = true;
            findesemana = false;
            esferiado = false;
            estaDisponible = 0;
            return;
        }
        for (Feriado f : horario.getFeriados()) {
            String feri = sdff.format(f.getFecha());
            if (sdff.format(fecha).equals(feri)) {
                esferiado = true;
                findesemana = false;
                fuerahorario = false;
                estaDisponible = 0;
                return;
            }
        }
        
        if (sdff.format(fecha).equals(sdff.format(fechaHoy)) || fecha.after(fechaHoy)) {
            fechaMenoraHoy = false;
            esferiado = false;
            findesemana = false;
            fuerahorario = false;
        } else {
            fechaMenoraHoy = true;
            fuerahorario = false;
            estaDisponible = 0;
            return;
        }
        if (horaDesde.after(horaHasta) || horaDesde.equals(horaHasta)) {
            fechadesdeMayorhasta = true;
            estaDisponible = 0;
            return;
        } else {
            fechadesdeMayorhasta = false;
            fechaMenoraHoy = false;
            esferiado = false;
            fuerahorario = false;
            findesemana = false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        reserva.setFecha(sdf.format(fecha));
        reserva.setHoraHasta(sdf.format(horaHasta));
        reserva.setHoraDesde(sdf.format(horaDesde));
        
        reserva.setPresupuesto(presupuesto);
        Persona p = service.getDisponible(reserva);
        estaDisponible = p == null ? 1 : 3;
    }
    
    public void seleccionarCliente(Persona p) {
        this.cliente = p;
    }
    
    public String add() {
        init();
        return "reserva";
    }
    
    public String volver() {
        return "reservaList";
    }
    
    public String edit() {
        cliente = reserva.getCliente();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fecha = sdff.parse(reserva.getFecha());
            horaDesde = sdf.parse(reserva.getHoraDesde());
            horaHasta = sdf.parse(reserva.getHoraHasta());
        } catch (Exception e) {
            fecha = new Date();
            horaDesde = new Date();
            horaHasta = new Date();
        }
        coloresSeleccionados = new ArrayList();
        presupuesto = reserva.getPresupuesto();
        presupuesto.getProductos().forEach(p -> {
            if (coloresSeleccionados != null && p.getCategoria().getIdCategoria() == 3) {
                coloresSeleccionados.add(p);
            } else {
                if (p.getCategoria().getIdCategoria() == 3) {
                    coloresSeleccionados = new ArrayList();
                    coloresSeleccionados.add(p);
                }
            }
        });
        zonaSeleccionada = presupuesto.getZona();
        coloresSeleccionados.forEach(p -> {
            colors.remove(p);
        });
        return "reserva";
    }
    
    public List<Reserva> getReservas() {
        return reservas;
    }
    
    public void setReservas(List<Reserva> reservas) {
        this.reservas = reservas;
    }
    
    public Reserva getReserva() {
        return reserva;
    }
    
    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }
    
    public Presupuesto getPresupuesto() {
        return presupuesto;
    }
    
    public void setPresupuesto(Presupuesto presupuesto) {
        this.presupuesto = presupuesto;
    }
    
    public List<Producto> getColors() {
        return colors;
    }
    
    public void setColors(List<Producto> colors) {
        this.colors = colors;
    }
    
    public List<Param> getZonas() {
        return zonas;
    }
    
    public void setZonas(List<Param> zonas) {
        this.zonas = zonas;
    }
    
    public Integer getZonaSeleccionada() {
        return zonaSeleccionada;
    }
    
    public void setZonaSeleccionada(Integer zonaSeleccionada) {
        this.zonaSeleccionada = zonaSeleccionada;
    }
    
    public List<Producto> getColoresSeleccionados() {
        return coloresSeleccionados;
    }
    
    public void setColoresSeleccionados(List<Producto> coloresSeleccionados) {
        this.coloresSeleccionados = coloresSeleccionados;
    }
    
    public boolean isVerBloqueGenerar() {
        return verBloqueGenerar;
    }
    
    public void setVerBloqueGenerar(boolean verBloqueGenerar) {
        this.verBloqueGenerar = verBloqueGenerar;
    }
    
    public List<Producto> getProductos() {
        return productos;
    }
    
    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    public CroppedImage getCroppedImage() {
        return croppedImage;
    }
    
    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }
    
    public UploadedFile getOriginalImageFile() {
        return originalImageFile;
    }
    
    public void setOriginalImageFile(UploadedFile originalImageFile) {
        this.originalImageFile = originalImageFile;
    }
    
    public Persona getCliente() {
        return cliente;
    }
    
    public void setCliente(Persona cliente) {
        this.cliente = cliente;
    }
    
    public List<Persona> getClientes() {
        return clientes;
    }
    
    public void setClientes(List<Persona> clientes) {
        this.clientes = clientes;
    }
    
    public Date getHoraDesde() {
        return horaDesde;
    }
    
    public void setHoraDesde(Date horaDesde) {
        this.horaDesde = horaDesde;
    }
    
    public void setEstaDisponible(int estaDisponible) {
        this.estaDisponible = estaDisponible;
    }
    
    public int getEstaDisponible() {
        return estaDisponible;
    }
    
    public int getPaneles() {
        return paneles;
    }
    
    public void setPaneles(int paneles) {
        this.paneles = paneles;
    }
    
    public Date getFecha() {
        return fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public Date getHoraHasta() {
        return horaHasta;
    }
    
    public void setHoraHasta(Date horaHasta) {
        this.horaHasta = horaHasta;
    }
    
    public boolean isFechadesdeMayorhasta() {
        return fechadesdeMayorhasta;
    }
    
    public void setFechadesdeMayorhasta(boolean fechadesdeMayorhasta) {
        this.fechadesdeMayorhasta = fechadesdeMayorhasta;
    }
    
    public boolean isFechaMenoraHoy() {
        return fechaMenoraHoy;
    }
    
    public void setFechaMenoraHoy(boolean fechaMenoraHoy) {
        this.fechaMenoraHoy = fechaMenoraHoy;
    }
    
    public void handleFileUpload(FileUploadEvent event) {
        this.originalImageFile = null;
        this.croppedImage = null;
        UploadedFile file = event.getFile();
        if (file != null && file.getContent() != null && file.getContent().length > 0 && file.getFileName() != null) {
            this.originalImageFile = file;
        }
    }
    
    public void crop() {
        if (this.croppedImage == null || this.croppedImage.getBytes() == null || this.croppedImage.getBytes().length == 0) {
        } else {
        }
    }
    
    public void addColor(Producto c) {
        coloresSeleccionados.add(c);
        colors.remove(c);
    }
    
    public void eliminarColor(Producto c) {
        colors.add(c);
        coloresSeleccionados.remove(c);
    }
    
    public StreamedContent getImage() {
        return DefaultStreamedContent.builder()
                .contentType(originalImageFile == null ? null : originalImageFile.getContentType())
                .stream(() -> {
                    if (originalImageFile == null
                            || originalImageFile.getContent() == null
                            || originalImageFile.getContent().length == 0) {
                        return null;
                    }
                    
                    try {
                        return new ByteArrayInputStream(originalImageFile.getContent());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .build();
    }
    
    public StreamedContent getCropped() {
        return DefaultStreamedContent.builder()
                .contentType(originalImageFile == null ? null : originalImageFile.getContentType())
                .stream(() -> {
                    if (croppedImage == null
                            || croppedImage.getBytes() == null
                            || croppedImage.getBytes().length == 0) {
                        return null;
                    }
                    
                    try {
                        return new ByteArrayInputStream(this.croppedImage.getBytes());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .build();
    }
    
    public void calcularPresupuesto() {
        if (presupuesto.getAlto() != null
                && presupuesto.getLargo() != null
                && presupuesto.getLargo() != 0
                && presupuesto.getAlto() != 0
                && zonaSeleccionada != null
                && coloresSeleccionados != null) {
            List<Producto> l = serviceproducto.getProductoData("A").getProductos();
            Integer valor = presupuesto.getLargo() * presupuesto.getAlto();
            BigDecimal suma = new BigDecimal("0");
            presupuesto.setProductos(l);
            for (Producto p : l) {
                suma = suma.add(new BigDecimal(coloresSeleccionados.size()).multiply(p.getPrecioUnitario()));
            }
            
            presupuesto.setCantHoras(valor < 200 ? 1 : valor > 200 && valor < 750 ? 2 : 3);
            suma = suma.add(new BigDecimal("10000").multiply(new BigDecimal(valor < 200 ? 3 : valor > 200 && valor < 750 ? 5 : 10)));
            presupuesto.setTotal(suma.multiply(new BigDecimal("2")));
            presupuesto.setCostoHora(presupuesto.getTotal().divide(new BigDecimal(presupuesto.getCantHoras()), RoundingMode.CEILING));
            presupuesto.setSena(suma);
            presupuesto.setMonto(suma.multiply(new BigDecimal("2")));
            presupuesto.setEstado(1);
            presupuesto.setUsuario("");
            presupuesto.setZona(zonaSeleccionada);
            verBloqueGenerar = true;
            mostrarMensaje = false;
        } else {
            FacesContext.getCurrentInstance().addMessage("Error ", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "", "Verifique los datos requeridos"));
            mostrarMensaje = true;
            verBloqueGenerar = false;
        }
        
    }
    
    public String getFormatDate(Date date) {
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy/MM/dd");
        return sdff.format(date);
    }
    
    public String verFactura() {
        if (factura != null && factura.getEstado().equals("P")) {
            formaMsg = null;
            formasPago = new ArrayList();
            monto= factura.getImporte();
        }
        return "facturaVenta";
    }
    
    public void addForma() {
        BigDecimal montoAcumulado = BigDecimal.ZERO;
        if (monto.equals(BigDecimal.ZERO)) {
            formaMsg = "El monto debe ser mayor a cero";
        } else {
            if (formasPago != null && !formasPago.isEmpty()) {
                for (FormaPago fp : formasPago) {
                    montoAcumulado = montoAcumulado.add(fp.getImporte());
                }
                montoAcumulado = montoAcumulado.add(monto);
                if (montoAcumulado.equals(factura.getImporte()) || montoAcumulado.compareTo(factura.getImporte()) < 0) {
                    formasPago.add(new FormaPago(monto, formaTipo, getFormaDescription(formaTipo)));
                    monto = null;
                    formaTipo = null;
                    formaMsg = null;
                } else {
                    formaMsg = "Ya se esta superando el importe de la factura";
                }
            } else {
                montoAcumulado = montoAcumulado.add(monto);
                if (formasPago == null) {
                    formasPago = new ArrayList();
                }
                if (montoAcumulado.equals(factura.getImporte()) || montoAcumulado.compareTo(factura.getImporte()) < 0) {
                    formasPago.add(new FormaPago(monto, formaTipo, getFormaDescription(formaTipo)));
                    monto = null;
                    formaTipo = null;
                    formaMsg = null;
                } else {
                    formaMsg = "Ya se esta superando el importe de la factura";
                }
                
            }
        }
        
    }
    
    public String anular() {
        if (cajaBean.getIdCaja() == null) {
            formaMsg = "Error, no se puede anular la factura, no se hecho la apertura de caja";
            return null;
        }
        if (factura.getEstado().equals("P")) {
            facturaService.update("C", factura.getIdFactura());
            facturas = facturaService.getFacturasVenta().getData();
            return "facturaVentaList";
        }
        CajaDetalle cajaDetalle = new CajaDetalle();
        cajaDetalle.setDetalle("Se anula la factura nro: " + factura.getIdFactura());
        cajaDetalle.setEgreso(factura.getImporte().toBigInteger());
        cajaDetalle.setFactura(factura);
        cajaDetalle.setFecha(new Date());
        cajaDetalle.setIngreso(BigInteger.ZERO);
        cajaBean.saveDetalle(cajaDetalle);
        facturaService.update("C", factura.getIdFactura());
        facturas = facturaService.getFacturasVenta().getData();
        return "facturaVentaList";
    }
    
    public String guardarFormaPago() {
        if (cajaBean.getIdCaja() == null) {
            formaMsg = "Error, no se puede pagar la factura, no se hecho la apertura de caja";
            return null;
        }
        BigDecimal montoAcumulado = BigDecimal.ZERO;
        if (formasPago == null || formasPago.isEmpty()) {
            formaMsg = "Favor complete la forma de pago";
        } else {
            for (FormaPago fp : formasPago) {
                montoAcumulado = montoAcumulado.add(fp.getImporte());
            }
            if (montoAcumulado.equals(factura.getImporte())) {
                facturaService.saveFormaPago(factura.getIdFactura(), formasPago);
                formaMsg = null;
                facturas = facturaService.getFacturasVenta().getData();
                CajaDetalle cajaDetalle = new CajaDetalle();
                cajaDetalle.setDetalle(factura.getDescripcion());
                cajaDetalle.setEgreso(BigInteger.ZERO);
                cajaDetalle.setFactura(factura);
                cajaDetalle.setFecha(new Date());
                cajaDetalle.setIngreso(factura.getImporte().toBigInteger());
                cajaBean.saveDetalle(cajaDetalle);
                return "facturaVentaList";
            } else {
                formaMsg = "Error al guardar Factura, el monto acumulado no coincide con el monto total de la factura";
            }
        }
        
        return null;
    }
    
    public String getFormaDescription(String forma) {
        if (forma.equals("E")) {
            return "Efectivo";
        }
        if (forma.equals("C")) {
            return "Cheque";
        }
        if (forma.equals("TD")) {
            return "Tarjeta de debito";
        }
        if (forma.equals("TC")) {
            return "Tarjeta de credito";
        }
        return "";
    }
    
    public boolean isMostrarMensaje() {
        return mostrarMensaje;
    }
    
    public void setMostrarMensaje(boolean mostrarMensaje) {
        this.mostrarMensaje = mostrarMensaje;
    }
    
    public Boolean getProcesar() {
        return procesar;
    }
    
    public void setProcesar(Boolean procesar) {
        this.procesar = procesar;
    }
    
    public Boolean getSenado() {
        return ver;
    }
    
    public void setSenado(Boolean senado) {
        this.ver = senado;
    }
    
    public Boolean getFinalizar() {
        return finalizar;
    }
    
    public void setFinalizar(Boolean finalizar) {
        this.finalizar = finalizar;
    }
    
    public Boolean getCancelar() {
        return cancelar;
    }
    
    public void setCancelar(Boolean cancelar) {
        this.cancelar = cancelar;
    }
    
    public Factura getFactura() {
        return factura;
    }
    
    public void setFactura(Factura factura) {
        this.factura = factura;
    }
    
    public String getWarningProcesar() {
        return warningProcesar;
    }
    
    public void setWarningProcesar(String warningProcesar) {
        this.warningProcesar = warningProcesar;
    }
    
    public boolean isMostrarMsg() {
        return mostrarMsg;
    }
    
    public void setMostrarMsg(boolean mostrarMsg) {
        this.mostrarMsg = mostrarMsg;
    }
    
    public List<Factura> getFacturas() {
        return facturas;
    }
    
    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }
    
    public List<FormaPago> getFormasPago() {
        return formasPago;
    }
    
    public void setFormasPago(List<FormaPago> formasPago) {
        this.formasPago = formasPago;
    }
    
    public BigDecimal getMonto() {
        return monto;
    }
    
    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }
    
    public FormaPago getForma() {
        return forma;
    }
    
    public void setForma(FormaPago forma) {
        this.forma = forma;
    }
    
    public List<Banco> getBancos() {
        return bancos;
    }
    
    public void setBancos(List<Banco> bancos) {
        this.bancos = bancos;
    }
    
    public String getFormaTipo() {
        return formaTipo;
    }
    
    public void setFormaTipo(String formaTipo) {
        this.formaTipo = formaTipo;
    }
    
    public String getFormaMsg() {
        return formaMsg;
    }
    
    public void setFormaMsg(String formaMsg) {
        this.formaMsg = formaMsg;
    }
    
    public boolean isAnular() {
        return anular;
    }
    
    public void setAnular(boolean anular) {
        this.anular = anular;
    }
    
    public boolean isEsferiado() {
        return esferiado;
    }
    
    public void setEsferiado(boolean esferiado) {
        this.esferiado = esferiado;
    }
    
    public boolean isFuerahorario() {
        return fuerahorario;
    }
    
    public void setFuerahorario(boolean fuerahorario) {
        this.fuerahorario = fuerahorario;
    }
    
    public boolean isFindesemana() {
        return findesemana;
    }
    
    public void setFindesemana(boolean findesemana) {
        this.findesemana = findesemana;
    }
    
    public String getDateString() {
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        return sdff.format(factura.getFecha());
    }
    
    public String getDateString(Date date) {
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        return sdff.format(date);
    }
    
    public Date getDate() {
        return new Date();
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
}
