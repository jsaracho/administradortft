/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.admintft.model;

import java.util.Date;

/**
 *
 * @author user
 */
public class Reserva {
    private Integer id;
    private Integer horaDesde;
    private Integer horaHasta;
    private Persona idCliente;
    private Persona idTatuador;
    private Presupuesto presupuesto;
    private Date fechaMod;
    private Date fechaAlta;
    private String usuarioAlta;
    private String usuarioMod;

    public Reserva() {
    }

    public Reserva(Integer id, Integer horaDesde, Integer horaHasta, Persona idCliente, Persona idTatuador, Presupuesto presupuesto, Date fechaMod, Date fechaAlta, String usuarioAlta, String usuarioMod) {
        this.id = id;
        this.horaDesde = horaDesde;
        this.horaHasta = horaHasta;
        this.idCliente = idCliente;
        this.idTatuador = idTatuador;
        this.presupuesto = presupuesto;
        this.fechaMod = fechaMod;
        this.fechaAlta = fechaAlta;
        this.usuarioAlta = usuarioAlta;
        this.usuarioMod = usuarioMod;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHoraDesde() {
        return horaDesde;
    }

    public void setHoraDesde(Integer horaDesde) {
        this.horaDesde = horaDesde;
    }

    public Integer getHoraHasta() {
        return horaHasta;
    }

    public void setHoraHasta(Integer horaHasta) {
        this.horaHasta = horaHasta;
    }

    public Persona getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Persona idCliente) {
        this.idCliente = idCliente;
    }

    public Persona getIdTatuador() {
        return idTatuador;
    }

    public void setIdTatuador(Persona idTatuador) {
        this.idTatuador = idTatuador;
    }

    public Presupuesto getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(Presupuesto presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
        return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
        this.usuarioAlta = usuarioAlta;
    }

    public String getUsuarioMod() {
        return usuarioMod;
    }

    public void setUsuarioMod(String usuarioMod) {
        this.usuarioMod = usuarioMod;
    }
    
    
           
}
